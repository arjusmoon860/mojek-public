const mongoose = require("mongoose");
var integrationSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        required: true
    },
    platform: {
        type: String,
        required: true,
        enum: ["Vezgo", "Zerodha", "BuyUcoin"],
        default: "Vezgo"
    },
    apiKey: {
        iv: {
            type: String
        },
        content: {
            type: String
        }
    },
    apiSecret: {
        iv: {
            type: String
        },
        content: {
            type: String
        }
    },
    accessToken: {
        iv: {
            type: String
        },
        content: {
            type: String
        }
    },
    refreshToken: {
        iv: {
            type: String
        },
        content: {
            type: String
        }
    },
    accountID: {
        type: String
    },
    provider: {
        name: {
            type: String
        },
        display_name: {
            type: String
        },
        logo: {
            type: String
        },
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "providers",
            required: true
        }
    },
});

integrationSchema.set("timestamps", true);

const Integration = mongoose.model("integration", integrationSchema);

module.exports = Integration;