const mongoose = require("mongoose");
var providerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    display_name: {
        type: String,
        required: true
    },
    logo: {
        type: String,
    },
    instructions: {
        type: String
    },
    provider: {
        type: String,
        enum: ["Vezgo", "Zerodha", "BuyUcoin"],
        required: true
    },
    category: {
        type: String,
        required: true,
        enum: ["crypto", "stocks"]
    }
});

providerSchema.set("timestamps", true);

const Providers = mongoose.model("providers", providerSchema);

module.exports = Providers;