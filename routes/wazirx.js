const { Router } = require("express");
const router = Router();
const userAuth = require("../middlewares/user");
const mongoose = require("mongoose");
const moment = require("moment");
const Balances = require("../models/Balances");
const Assets = require("../models/Assets");

const providerController = require("../controller/providers");
const integrationController = require("../controller/integration");
const userController = require("../controller/user");
const wazirxController = require("../controller/wazirxController");
const crypto = require("../controller/crypto");

const createAssetObject = async (holding, userID, today) => {
  const obj = {
    user: userID,
    platform: "WAZIRX",
    name: holding.asset.toUpperCase(),
    amount: holding.free,
    lastUpdate: today,
  };
  return obj;
};

const createBalanceObject = async (
  holding,
  userID,
  accountID,
  providerAccount,
  today
) => {
  const obj = {
    user: userID,
    accountID,
    provider: {
      name: providerAccount.name,
      display_name: providerAccount.display_name,
      logo: providerAccount.logo,
      id: providerAccount.id,
    },
    ticker: holding.asset.toUpperCase(),
    provider_ticker: holding.asset.toUpperCase(),
    name: holding.asset.toUpperCase(),
    amount: holding.free,
    type: "CRYPTO",
    decimals: holding.free.split(".")[1].length,
    balanceDate: today,
    balanceUpdatedAt: new Date().getTime(),
  };
  return obj;
};

router.post("/update-holdings", userAuth, async (req, res) => {
  let userEmail = req.user.email;
  let userID = req.user._id;
  let providerID = req.body.providerID;
  try {
    // Get Account ID from the Integration Data
    const accountData = await integrationController.getAccoundIDFromProvider(
      userID,
      providerID
    );
    const accountID = accountData.accountID;
    if (!accountID) {
      return res
        .status(400)
        .send({ error: true, message: "Invalid Account Selected!" });
    }

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
    var yyyy = today.getFullYear();
    today = `${yyyy}-${mm}-${dd}`;

    const providerAccount = await providerController.getProvider(providerID);
    const key = crypto.decrypt(accountData.apiKey);
    const secretKey = crypto.decrypt(accountData.apiSecret);

    const resp = await wazirxController.getHoldings(key, secretKey);

    const holdings = resp.data;
    for (const holding of holdings) {
      const newBalance = await createBalanceObject(
        holding,
        userID,
        accountID,
        providerAccount,
        today
      );
      const newAsset = await createAssetObject(holding, userID, today);

      var assetObj = await Assets.findOne({
        user: userID,
        platform: "WAZIRX",
        name: holding.asset.toUpperCase(),
      });
      var balanceObj = await Balances.findOne({
        user: userID,
        accountID,
        balanceDate: today,
        name: holding.asset.toUpperCase(),
      });
      if (!balanceObj) {
        // if balance obj doesn't exist
        await Balances.create(newBalance);
      } else {
        Object.assign(balanceObj, newBalance);
        await balanceObj.save();
      }
      if (!assetObj) {
        await Assets.create(newAsset);
      } else {
        Object.assign(assetObj, newAsset);
        await assetObj.save();
      }
    }
    res.send({ status: true });
  } catch (error) {
    res.status(400).send({
      error: true,
      message:
        "Unable to update holdings at the moment. Please try again later.",
    });
  }
});

module.exports = router;
