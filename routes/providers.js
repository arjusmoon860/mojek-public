const { Router } = require("express");
const router = Router();
const Providers = require("../models/Providers");

router.post("/add-provider", async (req, res) => {
    const { name } = req.body
    const isProvider = await Providers.findOne({
        name
    });
    if (isProvider) {
        return res.status(400).send({ error: true, message: "Integration already exists!" })
    }
    const provider = new Providers(req.body);
    try {
        await provider.save();
        res.send(provider);
    } catch (error) {
        console.log(error);
        res.status(400).send({ error: true, message: "Unable to add a new Provider right now" });
    }
})

module.exports = router