const Providers = require("../models/Providers");

const getProviders = () => {
    return new Promise((resolve, reject) => {
        Providers.find().lean().exec((err, providers) => {
            if (err) {
                return reject(err)
            }
            resolve(providers)
        })
    })
}

const getProvider = (providerID) => {
    return new Promise((resolve, reject) => {
        Providers.findById(providerID).exec((err, provider) => {
            if (err) {
                return reject(err)
            }
            resolve(provider)
        })
    })
}

module.exports = {
    getProviders,
    getProvider
}