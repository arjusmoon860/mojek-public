
const jwt = require("jsonwebtoken");
const User = require("../models/User");

const userLoginInitAuth = async (req, res, next) => {
    try {
        const token = req.header("Authorization").replace("Bearer ", "");
        const decoded = jwt.verify(token, process.env.JWT_SALT);
        const user = await User.findOne({
            _id: decoded._id,
            "loginTokens.token": token,
        });
        if (!user) {
            throw new Error();
        }

        req.loginToken = token;
        req.user = user;

        next();
    } catch (e) {
        res.status(401).send({ error: true, message: "Token Expired!" });
    }
};

module.exports = userLoginInitAuth;